library("GenomicFeatures")
load("4_mutation_coordenates.Rdata")
mutations <- GRanges(seqnames=mutations_coordenate$cromosome, strand="*", ranges=IRanges(start=mutations_coordenate$start, end=mutations_coordenate$end))
txdb <- makeTxDbFromGFF(file="datos_gtf_ensemble_grch38/Homo_sapiens.GRCh38.91.chr.gtf")
saveDb(txdb, file="5_txdb_database.Db")
#para cargar la Db sería: txdb <- loadDb("5_txdb_database.Db")
source("asignar_exon_transcript_gene_to_mutation.R")
mutations_asignadas <- asignar_exon_tx_gene_ver(mutations, txdb)
#lo que tiene dentro la función de asignar_exon_tx_gene es
asignar_exon_tx_gene_ver <- function(mutations, txdb){
	all_data <- cds(txdb, columns=c("exon_name", "tx_name", "gene_id", "exon_rank"))
	#x <- countOverlaps(mutations, all_data)
	overlaps_ran <- findOverlaps(mutations, all_data)
	ranges_over <-  overlapsRanges(ranges(mutations), ranges(all_data), overlaps_ran)
	mutations_asignadas <- granges(mutations)[queryHits(overlaps_ran)]
	strand(mutations_asignadas) <- strand(all_data)[subjectHits(overlaps_ran),]
	mcols(mutations_asignadas) <- mcols(all_data)[subjectHits(overlaps_ran),]
	mcols(mutations_asignadas)$width_exon <- width(all_data)[subjectHits(overlaps_ran)]
	mcols(mutations_asignadas)$width_solap_exon <- ranges_over@width
	mutations_asignadas
}
#devuelve un GRange con los exones que contiene
save(mutations_asignadas, file="5_mutaciones_asignadas.Rdata")

x_exon <- unlist(mutations_asignadas$exon_name)
x_exon_rank <- unlist(mutations_asignadas$exon_rank)
x_tx <- unlist(mutations_asignadas$tx_name)

#puedo utilizar cualquiera de los campos (exon_name, exon_rank ó tx_name)
solo_exon_rank <- mutations_asignadas$exon_rank
count_repeat <- sapply(solo_exon_rank, FUN=function(X){length(X)})

x_chrom <- rep(as.character(seqnames(mutations_asignadas)), times=count_repeat)
x_start <- rep(start(mutations_asignadas), times=count_repeat)
x_end <- rep(end(mutations_asignadas), times=count_repeat)
x_strand <- rep(as.character(strand(mutations_asignadas)), times=count_repeat)
x_width <- rep(width(mutations_asignadas), times=count_repeat)
x_wexon <- rep(mutations_asignadas$width_exon, times=count_repeat)
x_solap <- rep(mutations_asignadas$width_solap_exon, times=count_repeat)

mutaciones_exon_trans <- data.frame(cromosome=x_chrom, start=x_start, end=x_end, width = x_width, strand=x_strand, transcript_ensembl=x_tx, exon_ensembl=x_exon, exon_rank=x_exon_rank, width_cds_exon = x_wexon, width_solap = x_solap)
mutaciones_exon_trans$cromosome <- as.character(mutaciones_exon_trans$cromosome)
mutaciones_exon_trans$strand <- as.character(mutaciones_exon_trans$strand)
mutaciones_exon_trans$transcript_ensembl <- as.character(mutaciones_exon_trans$transcript_ensembl)
mutaciones_exon_trans$exon_ensembl <- as.character(mutaciones_exon_trans$exon_ensembl)
colnames(mutaciones_exon_trans)[4] <- "width_mutation"
save(mutaciones_exon_trans, file="5_datos_exon_transcript.Rdata")

transcripts_found <- mcols(transcripts(txdb, columns=c("tx_name", "gene_id")))
transcripts_found <- data.frame(gene_ensembl = unlist(transcripts_found$gene_id), transcript_ensembl = unlist(transcripts_found$tx_name))
save(transcripts_found, file="5_transcripts_found.Rdata")

#de estos transcriptos seleccionar los que tienen alguna mutación

mutaciones_all_data <- merge(mutaciones_exon_trans, transcripts_found[transcripts_found$transcript_ensembl %in% mutaciones_exon_trans$transcript_ensembl,], sort=FALSE)

save(mutaciones_all_data, file="6_mutaciones_all_data.Rdata")

#poner a cada sample los datos encontrados de sus mutaciones
load("4_whole_cosmic_cancer_type.RData")
load("6_mutaciones_all_data.Rdata")

whole_cosmic_v84_grch38$cromosome <- as.character(whole_cosmic_v84_grch38$cromosome)
whole_cosmic_v84_grch38[whole_cosmic_v84_grch38$cromosome %in% "23", "cromosome"] <- "X"
whole_cosmic_v84_grch38[whole_cosmic_v84_grch38$cromosome %in% "24", "cromosome"] <- "Y"
whole_cosmic_v84_grch38[whole_cosmic_v84_grch38$cromosome %in% "25", "cromosome"] <- "MT"
whole_cosmic_v84_grch38$start <- as.integer(whole_cosmic_v84_grch38$start)
whole_cosmic_v84_grch38$end <- as.integer(whole_cosmic_v84_grch38$end)
save(whole_cosmic_v84_grch38, file="6_whole_cosmic_fix.Rdata")

whole_cosmic_coding <- merge(whole_cosmic_v84_grch38, mutaciones_all_data, sort=FALSE)
save(whole_cosmic_coding, file="7_whole_cosmic_coding.Rdata")

 whole_cosmic_coding_select <- unique(whole_cosmic_coding[, c("cromosome", "start", "end", "strand", "transcript_ensembl", "exon_ensembl", "exon_rank", "gene_ensembl", "ID_sample", "Mutation.CDS", "Mutation.Description", "GRCh", "Mutation.somatic.status", "Pubmed_PMID", "Sample.source", "Tumour.origin", "cancer_type", "width_cds_exon", "width_solap", "width_mutation")])
save(whole_cosmic_coding_select, file="7_whole_cosmic_coding_select_columns.Rdata")

