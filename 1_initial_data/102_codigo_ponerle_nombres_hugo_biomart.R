load("7_whole_cosmic_coding_select_columns.Rdata")

#ponerle los nombres de gene_name a cada gene_ensembl
library(biomaRt)

ensembl_us_west = useMart(biomart="ENSEMBL_MART_ENSEMBL", host="uswest.ensembl.org", dataset="hsapiens_gene_ensembl")

x <- unique(whole_cosmic_coding_select$gene_ensembl)
gene_name <- getBM(attributes=c('ensembl_gene_id','hgnc_symbol', 'hgnc_id'), filters = 'ensembl_gene_id', values = x, mart = ensembl_us_west)

#de cada transcript poner su uniprot de swissprot
y <- unique(whole_cosmic_coding_select$transcript_ensembl)
uniprot_name <- getBM(attributes=c('ensembl_transcript_id', 'uniprotswissprot', 'transcript_length', 'cds_length'), filters = 'ensembl_transcript_id', values = y, mart = ensembl_us_west)

#verificar que cada transcripto tenga un solo uniprot y si tiene más de uno dejar el más apropiado
s = aggregate(x = uniprot_name$uniprotswissprot, by=list(uniprot_name$ensembl_transcript_id), function(x){length(x)})
mas_1 <- uniprot_name[uniprot_name$ensembl_transcript_id %in% s[s$x > 1, "Group.1"], c("ensembl_transcript_id", "uniprotswissprot")]

#length(unique(mas_1$ensembl_transcript_id)) hay 12 transcriptos con más de un uniprot 
mas_1 <- mas_1[order(mas_1$ensembl_transcript_id),]
mas_1$delete <- NA
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000340457")) & (mas_1$uniprotswissprot %in% c("E5RIL1")), "delete"] <- "si"
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000360004")) & (mas_1$uniprotswissprot %in% c("Q29974", "Q9GIY3", "P04229")), "delete"] <- "si"
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000376809", "ENST00000396634")) & (mas_1$uniprotswissprot %in% c("P13746", "P16188")), "delete"] <- "si"
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000396265")) & (mas_1$uniprotswissprot %in% c("B1AH88")), "delete"] <- "si"
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000412585")) & (mas_1$uniprotswissprot %in% c("Q31610", "Q31612", "Q29836", "P30486", "P30480")), "delete"] <- "si"
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000440137", "ENST00000639028")) & (mas_1$uniprotswissprot %in% c("P0CL82","O76087")), "delete"] <- "si"
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000445148")) & (mas_1$uniprotswissprot %in% c("P0CL81","O76087")), "delete"] <- "si"
#los siguientes transcriptos necesitan dos uniprot porque uno es el inicio del transcripto y el otro es el fin de los transcriptos, eliminaremos uno de los uniprot y luego renombraremos el uniprot por P35544_P62861
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000529639", "ENST00000527548", "ENST00000531743")) & (mas_1$uniprotswissprot %in% c("P62861")), "delete"] <- "si"

mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000603660") & (mas_1$uniprotswissprot %in% c("P0DP03")), "delete"] <- "si"
mas_1[(mas_1$ensembl_transcript_id %in% c("ENST00000611391") & (mas_1$uniprotswissprot %in% c("P0DP09")), "delete"] <- "si"

uniprot_name <- merge(x=uniprot_name, mas_1, all.x=TRUE, sort=FALSE)
uniprot_name <- uniprot_name[is.na(uniprot_name$delete), c("ensembl_transcript_id", "uniprotswissprot", "transcript_length", "cds_length")]
#renombrar el uniprot de los transcriptos que necesitan los 2
uniprot_name[uniprot_name$ensembl_transcript_id %in% c("ENST00000529639", "ENST00000527548", "ENST00000531743"), "uniprotswissprot"] <- "P35544_P62861"

colnames(gene_name)[1] <-  "gene_ensembl"
colnames(uniprot_name)[1] <-  "transcript_ensembl"
save(uniprot_name, gene_name, file="7_other_data_genename_hgnc_uniprot.Rdata")

whole_cosmic_coding_select_all <- merge(x=whole_cosmic_coding_select, y=gene_name, sort=FALSE)
whole_cosmic_coding_select_all <- merge(whole_cosmic_coding_select_all, uniprot_name , sort=FALSE)

#cambiar las cadenas vacías de uniprotswissprot por NA
whole_cosmic_coding_select_all[whole_cosmic_coding_select_all$uniprotswissprot %in% "", "uniprotswissprot"] <- NA
save(whole_cosmic_coding_select_all, file="8_whole_cosmic_coding_select_all.Rdata")

