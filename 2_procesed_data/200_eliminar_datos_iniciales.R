load("10_whole_cosmic_fix_new.Rdata")
select_gene  <- unique(whole_cosmic_fixed[, c("cancer_type", "gene_ensembl", "ID_sample", "cds_length_transcripts")])

x <- unique(select_gene[, c("cancer_type", "ID_sample")])
count_mut_sample = aggregate(x = x$ID_sample, by=list(cancer_type = x$cancer_type), function(x){length(x)})
colnames(count_mut_sample)[2] <- "total_samples"

count_gene_per_sample = aggregate(x = select_gene$ID_sample, by=list(gene_ensembl=select_gene$gene_ensembl, cancer_type= select_gene$cancer_type, cds_length_transcripts = select_gene$cds_length_transcripts), function(x){length(x)})
colnames(count_gene_per_sample)[4] <- "n_samples"
count_gene_per_sample$total_samples <- count_mut_sample[count_gene_per_sample$cancer_type, "total_samples"]

#poner a cada gene su frecuencia, importancia y importancia2
count_gene_per_sample$frequency <- count_gene_per_sample$n_samples / count_gene_per_sample$total_samples

#calcular importancia como #samples / max(1, length_pd/cuota)
#poner la cuota como el # de pares de bases para definir outlier que es 3 ves la distancia intercuartil + el 3er cuartil

library("GenomicFeatures")
txdb <- loadDb("../1_inicial_datos_best/5_txdb_database.Db")
all_data <- cdsBy(txdb, by=c("gene"))
#tengo 20291 genes que tiene parte codificante length(all_data)
datos <- disjoin(all_data) #junto las partes codificantes de todos los transcriptos por gen
length_cds <- sum(width(datos)) #longitud de la parte codificante de cada gen
summary(length_cds)
 Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
      8       828       1341    1780      2139  114595 
quantile(length_cds, prob = c(0.25, 0.50, 0.75, 0.95))
   25%    50%    75%    95% 
 828.0 1341.0 2139.0 4559.5
3* IQR(length_cds) #3933 = 3*(2139-828)
#es considerando outlier los que tienen más de  6072 (3933+2139) pb codificantes
#o solo penalizar el 1% de los genes
quantile(length_cds, c(.25, .50, .75, .90, .95, .99, .995, .9995, .9999))

k <- data.frame(gene=names(length_cds), length_pb = length_cds, row.names=NULL)
save(k, file="40_datos_density_genes_length.Rdata")
write.table(k, file="40_length_20291_genes.csv", sep="\t", quote=FALSE, row.names=FALSE )
t_gene <- (k[k$length_pb > 18928,])

#importancia escala la cantidad de samples de acuerdo a la longitud del gen, si el gen es 2 veces más largo que lo considerado outlier se cuenta como la mitad de las samples, y así sucesivamente.
count_gene_per_sample$importancia <- ceiling(count_gene_per_sample$n_samples / apply(matrix(count_gene_per_sample$cds_length_transcripts), 1, FUN=function(X){
max(1, X/6072)
}) )
save(count_gene_per_sample, file="40_importance_gene.Rdata")
#cantidad mínima de samples para dejar un gen, que corresponde como mínimo a 5 samples (o más del 1% de las samples en cada tipo de cáncer)
count_gene_per_sample$sample_1_percent <- apply(matrix(count_gene_per_sample$total_samples), 1, FUN=function(X){max(5, floor(X/100))})

gene_sel2 <- count_gene_per_sample[count_gene_per_sample$importancia > count_gene_per_sample$sample_1_percent, ]
subset_select <- merge(gene_sel2, select_gene, sort=FALSE)
save(subset_select, file="40_subset.Rdata")
#quedarían 22172
#además eliminar los 11 genes más largos que correponde al 0.05% de los genes y tienen más de 18928 pares de bases
subset_select <- subset_select[!(subset_select$gene_ensembl %in% as.character(t_gene$gene)),]
save(subset_select, file="40_subset_sin.Rdata")
#quedarían 22166 samples

x2 <- unique(subset_select[, c("cancer_type", "ID_sample")])
count_mut_sample2 = aggregate(x = x2$ID_sample, by=list(cancer_type = x2$cancer_type), function(x){length(x)})
colnames(count_mut_sample2)[2] <- "total_samples_clean"
count_mut_sample2 <- merge(count_mut_sample, count_mut_sample2)
count_mut_sample2$percent <- count_mut_sample2$total_samples_clean/ count_mut_sample2$total_samples
write.table(count_mut_sample2, file="40_samples_per_filtergene_sin.csv", quote=FALSE, row.names=FALSE)

