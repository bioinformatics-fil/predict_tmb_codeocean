load("40_importance_gene.Rdata")
y <- unique(count_gene_per_sample[, c("cancer_type", "importancia", "total_samples", "gene_ensembl")])
load("43_subset_panel.Rdata")
whole_cosmic_select <- merge(x=whole_cosmic_select, y, x.all=TRUE)
save(whole_cosmic_select, file="43_subset_gene_panel.Rdata")
		
		load("43_subset_census.Rdata")
whole_cosmic_select <- merge(x=whole_cosmic_select, y, x.all=TRUE)
save(whole_cosmic_select, file="43_subset_gene_census.Rdata")

load("43_subset_gene_panel.Rdata")
colnames(whole_cosmic_select)[28] <- "importancia_gene"
select_exon <- unique(whole_cosmic_select[, c("exon_ensembl", "importancia_gene", "total_samples", "width_cds_exon", "cds_length_transcripts", "cancer_type", "ID_sample")])
count_exon_per_sample = aggregate(x = select_exon$ID_sample, by=list(exon_ensembl=select_exon$exon_ensembl, cancer_type= select_exon$cancer_type, cds_length_transcripts = select_exon$cds_length_transcripts, total_samples = select_exon$total_samples, width_cds_exon=select_exon$width_cds_exon, importancia_gene = select_exon$importancia_gene), function(x){length(x)})
colnames(count_exon_per_sample)[7] <- "n_samples"
#frequency
#frequency
count_exon_per_sample$frequency <- count_exon_per_sample$n_samples/ count_exon_per_sample$total_samples
#importancia exon es la cantidad mínima de mutaciones en el exon
count_exon_per_sample$importancia_exon <- count_exon_per_sample$importancia_gene / (count_exon_per_sample$cds_length_transcripts / count_exon_per_sample$width_cds_exon)
#importancia tendría un valor que dice cuantas veces tengo más mutaciones en el exon de los que debería tener teniendo en cuenta su longitud y el pesado del gene
count_exon_per_sample$importancia <- (count_exon_per_sample$frequency * count_exon_per_sample$importancia_gene) / sapply(count_exon_per_sample$importancia_exon, FUN=max, 1)
#se hace lo del máximo porque: importancia_exon puede ser menor que 1 (sample) y (freq x importancia gene también pude ser menor que 1) 
y <- unique(count_exon_per_sample[, c("cancer_type", "importancia", "exon_ensembl")])
whole_cosmic_select <- merge(x=whole_cosmic_select, y, x.all=TRUE)
save(whole_cosmic_select, file="43_subset_exon_panel.Rdata")

#exon en census
load("43_subset_gene_census.Rdata")
colnames(whole_cosmic_select)[28] <- "importancia_gene"
select_exon <- unique(whole_cosmic_select[, c("exon_ensembl", "importancia_gene", "total_samples", "width_cds_exon", "cds_length_transcripts", "cancer_type", "ID_sample")])
count_exon_per_sample = aggregate(x = select_exon$ID_sample, by=list(exon_ensembl=select_exon$exon_ensembl, cancer_type= select_exon$cancer_type, cds_length_transcripts = select_exon$cds_length_transcripts, total_samples = select_exon$total_samples, width_cds_exon=select_exon$width_cds_exon, importancia_gene = select_exon$importancia_gene), function(x){length(x)})
colnames(count_exon_per_sample)[7] <- "n_samples"
#frequency
count_exon_per_sample$frequency <- count_exon_per_sample$n_samples/ count_exon_per_sample$total_samples
#importancia exon es la cantidad mínima de mutaciones en el exon
count_exon_per_sample$importancia_exon <- count_exon_per_sample$importancia_gene / (count_exon_per_sample$cds_length_transcripts / count_exon_per_sample$width_cds_exon)
#importancia tendría un valor que dice cuantas veces tengo más mutaciones en el exon de los que debería tener teniendo en cuenta su longitud y el pesado del gene
count_exon_per_sample$importancia <- (count_exon_per_sample$frequency * count_exon_per_sample$importancia_gene) / sapply(count_exon_per_sample$importancia_exon, FUN=max, 1)
#se hace lo del máximo porque: importancia_exon puede ser menor que 1 (sample) y (freq x importancia gene también pude ser menor que 1) 
y <- unique(count_exon_per_sample[, c("cancer_type", "importancia", "exon_ensembl")])
whole_cosmic_select <- merge(x=whole_cosmic_select, y, x.all=TRUE)
save(whole_cosmic_select, file="43_subset_exon_census.Rdata")
