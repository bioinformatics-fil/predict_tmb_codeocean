tabla_describe_sample_gene <- function(data_agregate, data_sinaggregate){
	d <- data.frame()
	cancer_types <- as.character(unique(data_agregate$cancer_type))
	for(cancer in cancer_types){
		k <- data.frame()
		dat_first <- data_agregate[data_agregate$cancer_type %in% cancer,]
		dat_first <- dat_first[order(-dat_first$importancia), ]
		dat_first_gene <- dat_first$gene_ensembl
		dat_sin <- data_sinaggregate[data_sinaggregate$cancer_type %in% cancer,]
		#camenzar desde la cantidad máxima de genes que tiene hasta 1 y guardar el % de samples que los contienen
		max_gene <- length(unique(dat_first_gene)) 
		max_sample <- length(unique(dat_sin$ID_sample)) 
		#for(first_a in c(max_gene:1)){
		#	dat_first <- dat_first[1:first_a]
		#	dat_sin <- dat_sin[dat_sin$gene_name %in% dat_first, ]
		#	x <- length(unique(dat_sin$ID_sample))
		#	d <- rbind(d, data.frame(cancer_type = cancer, max_sample = max_sample, n_genes = first_a, samples = x, percent= (x/max_sample)))
		#}
		j <- apply(data.frame(x=c(1:max_gene)), 1, FUN=function(X){length(unique(dat_sin[dat_sin$gene_ensembl %in% dat_first_gene[1:X], "ID_sample"]))})
		d <- rbind(d, data.frame(cancer_type = cancer, max_sample = max_sample, n_genes = c(1:max_gene), samples = j, acum_percent= (j/max_sample), gene_add = dat_first_gene[1:max_gene], samples_importancia=dat_first[c(1:max_gene),"importancia"], percent = dat_first[c(1:max_gene),"frequency"]))
	}
	d
}

tabla_describe_sample_exon <- function(data_agregate, data_sinaggregate){
	d <- data.frame()
	cancer_types <- as.character(unique(data_agregate$cancer_type))
	for(cancer in cancer_types){
		dat_first <- data_agregate[data_agregate$cancer_type %in% cancer,]
		dat_first <- dat_first[order(-dat_first$importancia), ]
		dat_first_exon <- as.character(unique(dat_first$exon_ensembl))
		dat_sin <- data_sinaggregate[data_sinaggregate$cancer_type %in% cancer,]
		#camenzar desde la cantidad máxima de genes que tiene hasta 1 y guardar el % de samples que los contienen
		max_exon <- length(dat_first_exon) 
		max_sample <- length(unique(dat_sin$ID_sample)) 
		#for(first_a in c(max_mutation:1)){
		#	dat_first <- dat_first[1:first_a]
		#	dat_sin <- dat_sin[dat_sin$Mutation.ID %in% dat_first, ]
		#	x <- length(unique(dat_sin$ID_sample))
		#	d <- rbind(d, data.frame(cancer_type = cancer, max_sample = max_sample, n_mutation = first_a, samples = x, percent= (x/max_sample)))
		#}
		j <- apply(data.frame(x=c(1:max_exon)), 1, FUN=function(X){length(unique(dat_sin[dat_sin$exon_ensembl %in% dat_first_exon[1:X], "ID_sample"]))})
		d <- rbind(d, data.frame(cancer_type = cancer, max_sample = max_sample, n_exon = c(1:max_exon), samples = j, acum_percent= (j/max_sample), exon_add = dat_first_exon[1:max_exon], importancia=dat_first[c(1:max_exon),"importancia"], percent = dat_first[c(1:max_exon),"frequency"]))
	}
	d
}


tabla_describe_sample_gene_nsample <- function(data_agregate, data_sinaggregate){
    d <- data.frame()
    cancer_types <- as.character(unique(data_agregate$cancer_type))
    for(cancer in cancer_types){
        k <- data.frame()
        dat_first <- data_agregate[data_agregate$cancer_type %in% cancer,]
        dat_first <- dat_first[order(-dat_first$n_samples), ]
        dat_first_gene <- dat_first$gene_ensembl
        dat_sin <- data_sinaggregate[data_sinaggregate$cancer_type %in% cancer,]
        #camenzar desde la cantidad máxima de genes que tiene hasta 1 y guardar el % de samples que los contienen
        max_gene <- length(unique(dat_first_gene))
        max_sample <- length(unique(dat_sin$ID_sample))
        #for(first_a in c(max_gene:1)){
        #	dat_first <- dat_first[1:first_a]
        #	dat_sin <- dat_sin[dat_sin$gene_name %in% dat_first, ]
        #	x <- length(unique(dat_sin$ID_sample))
        #	d <- rbind(d, data.frame(cancer_type = cancer, max_sample = max_sample, n_genes = first_a, samples = x, percent= (x/max_sample)))
        #}
        j <- apply(data.frame(x=c(1:max_gene)), 1, FUN=function(X){length(unique(dat_sin[dat_sin$gene_ensembl %in% dat_first_gene[1:X], "ID_sample"]))})
        d <- rbind(d, data.frame(cancer_type = cancer, max_sample = max_sample, n_genes = c(1:max_gene), samples = j, acum_percent= (j/max_sample), gene_add = dat_first_gene[1:max_gene], percent = dat_first[c(1:max_gene),"frequency"]))
    }
    d
}




#los 500 genes más frecuentes
# 500_genes_first <- stat_plot_gene[stat_plot_gene$n_genes <= 500, c("cancer_type", "gene_name")]
# 500_genes_first$tipo <- "genes_500"

#los genes más frecuentes que describen el 90% de las samples
# gene_sel <- unique(sel_90_sample_gene[, c("cancer_type", "gene_name")])
# gene_sel$tipo <- "gene_90"

#los genes de las primeras mutaciones más frecuentes que definen el 90% de las samples
# sel_90_sample_mutation <- sel_X_sample_mutation(stat_plot_mutation, 0.9)
# genes_mutation_first <- unique(sel_90_sample_mutation[, c("cancer_type", "mutation_add")])
# colnames (genes_mutation_first)[2] <- "Mutation.ID"
# load("7_select_whole_mapping_mutation.RData")
# gene_mut_sel <- unique(select_whole_mapping_mutation[, c("cancer_type", "Mutation.ID", "gene_name")])
# gene_mut_sel <- merge(gene_mut_sel, genes_mutation_first)
# gene_mut_sel <- unique(gene_mut_sel[, c("cancer_type", "gene_name")]) 
# gene_mut_sel$tipo <- "gene_mut_90"

#seleccionar los primeros genes que cubren al menor el X porciento de las samples
# X_percent <- 0.90
sel_X_sample_gene <- function(stat_plot_gene, X_percent=0.9){
	d <- data.frame()
	cancer_types <- as.character(unique(stat_plot_gene$cancer_type))
	for(cancer in cancer_types){		
		dat_first <- stat_plot_gene[stat_plot_gene$cancer_type == cancer,]
		dat_first <- dat_first[order(dat_first$acum_percent), ]
		# por si la cantidad de porciento a pedir es menor que el % acumulativo del primer gene 
		if(min(dat_first$acum_percent) > X_percent){
			d <- rbind(d, dat_first[dat_first$n_genes == 1,])
		}else{
			tope <- min(X_percent, max(dat_first$acum_percent))
			k <- dat_first[dat_first$acum_percent < tope, ]
			d <- rbind(d, k, dat_first[dat_first$n_genes == (max(k$n_genes) + 1),])
		}		
	}
	d
}

#seleccionar los primeros exones que cubren al menor el X porciento de las samples
# X_percent <- 0.90
sel_X_sample_exon <- function(stat_plot_mutation_exon, X_percent=0.9){
	d <- data.frame()
	cancer_types <- as.character(unique(stat_plot_mutation_exon$cancer_type))
	for(cancer in cancer_types){
		dat_first <- stat_plot_mutation_exon[stat_plot_mutation_exon$cancer_type == cancer,]
		dat_first <- dat_first[order(dat_first$acum_percent), ]
		# por si la cantidad de porciento a pedir es menor que el % acumulativo del primer exon 
		if(min(dat_first$acum_percent) > X_percent){
			d <- rbind(d, dat_first[dat_first$n_exon == 1,])
		}else{
			tope <- min(X_percent, max(dat_first$acum_percent))
			#d <- rbind(d, dat_first[dat_first$acum_percent < X_percent,])
			k <- dat_first[dat_first$acum_percent < tope,]
			d <- rbind(d, k, dat_first[dat_first$n_exon == (max(k$n_exon) + 1),])
		}
	}
	d
}


select_X_first_frequency <- function(data, x_first=100){
	d <- data.frame()
	cancer_types <- as.character(unique(data$cancer_type))
	for(cancer in cancer_types){
		x <- data[data$cancer_type == cancer,]
		x <- x[order(-x$frequency),]
		max <- x_first
		if(length(x[,1]) < x_first){
			max <- length(x[,1])
		}
		d <- rbind(d, x[1:max,])
	}
	d
}



