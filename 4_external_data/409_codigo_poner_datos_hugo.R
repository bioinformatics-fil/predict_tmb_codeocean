#Eliminar los dos paper en común decir 133 paper.

juntar los datos externos de paper con los datos externos de cosmic
load("4_ponerle_todos_los_datos/4_data_external_paper.Rdata")
load("5_datos_cosmic_v90/5_5_whole_cosmic_v90_cancer_type_selcolumns.Rdata")
all_external <- rbind(datos_cosmic_v90, data_external_paper[!(data_external_paper$pubmed %in% c(29925043, 27340278)), c("sample", "cancer_type", "pubmed", "cromosoma", "start", "end")])
all_external <- unique(all_external)
save(all_external, file="6_poner_datos_extra_todos/6_0_allexternal_data.Rdata")
hacer unique de los datos para eliminar cualquier mutación repetida por sample
#quedaron 8182 sample, donde 4786 son de artículos y 3396 son nuevas de cosmic v90

ponerle a cada coordenada su gene ensembl, transcript ensembl, exon ensembl, hugo, uniprot, etc.. y quedarme con las mutaciones que son en exones
load("6_0_allexternal_data.Rdata")
mutations_coordenate <- unique(all_external[, c("cromosoma", "start", "end")])
save(mutations_coordenate, file="6_1_mutation_coordenates.Rdata")
library("GenomicFeatures")
load("6_1_mutation_coordenates.Rdata")
txdb <- loadDb("5_txdb_database.Db")
mutations <- GRanges(seqnames=mutations_coordenate$cromosoma, strand="*", ranges=IRanges(start=mutations_coordenate$start, end=mutations_coordenate$end))
asignar_exon_tx_gene_ver <- function(mutations, txdb){
	all_data <- cds(txdb, columns=c("exon_name", "tx_name", "gene_id", "exon_rank"))
	#x <- countOverlaps(mutations, all_data)
	overlaps_ran <- findOverlaps(mutations, all_data)
	ranges_over <-  overlapsRanges(ranges(mutations), ranges(all_data), overlaps_ran)
	mutations_asignadas <- granges(mutations)[queryHits(overlaps_ran)]
	strand(mutations_asignadas) <- strand(all_data)[subjectHits(overlaps_ran),]
	mcols(mutations_asignadas) <- mcols(all_data)[subjectHits(overlaps_ran),]
	mcols(mutations_asignadas)$width_exon <- width(all_data)[subjectHits(overlaps_ran)]
	mcols(mutations_asignadas)$width_solap_exon <- ranges_over@width
	mutations_asignadas
}
mutations_asignadas <- asignar_exon_tx_gene_ver(mutations, txdb)

#devuelve un GRange con los exones que contiene
save(mutations_asignadas, file="6_2_mutaciones_asignadas.Rdata")

#arreglar los datos de GRange para obtener de cada mutación una fila para cada relacion exon, transcript gene_name. Debido a que cuando obtengo los cds el campo de gene_id puede contener varios y no se especifica de que transcripto es cada uno, Hacemos un data.frame con los datos de mutacion, exon, y transcript y hacemos otro data.frame con transcript y gene y los combinamos.

x_exon <- unlist(mutations_asignadas$exon_name)
x_exon_rank <- unlist(mutations_asignadas$exon_rank)
x_tx <- unlist(mutations_asignadas$tx_name)
solo_exon_rank <- mutations_asignadas$exon_rank
count_repeat <- sapply(solo_exon_rank, FUN=function(X){length(X)})

x_chrom <- rep(as.character(seqnames(mutations_asignadas)), times=count_repeat)
x_start <- rep(start(mutations_asignadas), times=count_repeat)
x_end <- rep(end(mutations_asignadas), times=count_repeat)
x_strand <- rep(as.character(strand(mutations_asignadas)), times=count_repeat)
x_width <- rep(width(mutations_asignadas), times=count_repeat)
x_wexon <- rep(mutations_asignadas$width_exon, times=count_repeat)
x_solap <- rep(mutations_asignadas$width_solap_exon, times=count_repeat)

mutaciones_exon_trans <- data.frame(cromosoma=x_chrom, start=x_start, end=x_end, width = x_width, strand=x_strand, transcript_ensembl=x_tx, exon_ensembl=x_exon, exon_rank=x_exon_rank, width_cds_exon = x_wexon, width_solap = x_solap)
mutaciones_exon_trans$cromosoma <- as.character(mutaciones_exon_trans$cromosoma)
mutaciones_exon_trans$strand <- as.character(mutaciones_exon_trans$strand)
mutaciones_exon_trans$transcript_ensembl <- as.character(mutaciones_exon_trans$transcript_ensembl)
mutaciones_exon_trans$exon_ensembl <- as.character(mutaciones_exon_trans$exon_ensembl)
colnames(mutaciones_exon_trans)[4] <- "width_mutation"
save(mutaciones_exon_trans, file="6_2_datos_exon_transcript.Rdata")
transcripts_found <- mcols(transcripts(txdb, columns=c("tx_name", "gene_id")))
transcripts_found <- data.frame(gene_ensembl = unlist(transcripts_found$gene_id), transcript_ensembl = unlist(transcripts_found$tx_name))
save(transcripts_found, file="6_2_transcripts_found.Rdata")

mutaciones_all_data <- merge(mutaciones_exon_trans, transcripts_found[transcripts_found$transcript_ensembl %in% mutaciones_exon_trans$transcript_ensembl,], sort=FALSE)
save(mutaciones_all_data, file="6_3_mutaciones_all_data.Rdata")

whole_coding <- merge(all_external, mutaciones_all_data, sort=FALSE)
save(whole_coding, file="6_4_whole_coding.Rdata")

whole_coding_fixed <- unique(whole_coding[, c("cromosoma", "start", "end", "sample", "cancer_type", "pubmed", "strand", "exon_ensembl", "gene_ensembl")])
save(whole_coding_fixed, file="6_7_whole_fix.Rdata")
