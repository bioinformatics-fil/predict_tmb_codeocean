load("6_7_alldata_v90/56_consensus_filtered_length.Rdata")

t_cancer <- unique(k_data_tabla[k_data_tabla$selection %in% "oursin" & k_data_tabla$tipo %in% "gene" & !(k_data_tabla$variable %in% "(Intercept)"), c("cancer_type", "variable")])
ensembl_prot <- unique(t_cancer$variable)
max_prot <- length(ensembl_prot)
#hay 1239 ensembl

library("biomaRt" )
mart = useMart("ensembl", dataset="hsapiens_gene_ensembl")
t_go <- getBM(attributes = c("ensembl_gene_id", "go_id", "name_1006", "definition_1006", "namespace_1003"), filters = "ensembl_gene_id", values = ensembl_prot,mart = mart)

#ver si se encontró Go cada ensembl
#se encontró Go para 1238 ensembl. No encontrado para "ENSG00000274267"

#eliminar las filas donde no tiene id de término de go.
t_go <- t_go[!(t_go$go_id %in% ""), ]
#quedaron 1228 ensembl con algún go_id
#quedaron 6184 término de go.

t_agregado <- aggregate(t_go$ensembl_gene_id, by=list(go_id=t_go$go_id, go_name=t_go$name_1006), FUN=length) 
t_agregado <- t_agregado[order(-t_agregado$x), ]

#Ejemplo:
#termino						name	      proteínas
#GO:0016301                                           	   kinase activity  73
#GO:0004672                                   protein kinase activity  64

save(t_cancer, t_go, t_agregado, file="6_9_GO/80_term_go.Rdata")


library("reshape2")
load("80_term_go.Rdata")
load("../6_7_alldata_v90/56_consensus_filtered_length.Rdata")
#poner a t_cancer la cantidad de genes que tiene

t_go$go <- paste0(t_go$go_id, ";", t_go$name_1006)
t_go$value <- "X"
a_by_go <- aggregate(t_go$ensembl_gene_id, by=list(go=t_go$go), FUN=length)
colnames(a_by_go)[2] <- "total"
a_by_go <- a_by_go[order(-a_by_go$total), ]

#no poner todos los go solo los que están presentes en 5 o más genes
a_by_go <- a_by_go[a_by_go$total >= 5, ]

t_go <- merge(t_go, a_by_go)
t_go$go <- factor(t_go$go, levels=a_by_go$go)
go_term <- dcast(data=t_go[, c("ensembl_gene_id", "go", "value", "total")], go + total ~ ensembl_gene_id, value.var="value", fill="")

a_by_cancer <- aggregate(t_cancer$cancer_type, by=list(variable=t_cancer$variable), FUN=length)
colnames(a_by_cancer)[2] <- "cancer"

#para poder ordenar los genes
t_model <- unique(k_data_tabla[k_data_tabla$selection %in% "oursin" & k_data_tabla$tipo %in% "gene" & !(k_data_tabla$variable %in% "(Intercept)"), c("cancer_type", "variable", "model_number")])
t_model <- aggregate(t_model$model_number, by=list(variable=t_model$variable), FUN=length)
colnames(t_model)[2] <- "model"
a_by_cancer <- merge(a_by_cancer, t_model)

a_by_cancer$tiene_go <- 0
a_by_cancer[a_by_cancer$variable %in% t_go$ensembl_gene_id, "tiene_go"] <- 1

a_by_cancer <- a_by_cancer[order(-a_by_cancer$tiene_go, -a_by_cancer$cancer, -a_by_cancer$model),]

#a go_term le faltan algunas proteinas y otras faltan porque solo se seleccionaron algunos go >= 5 genes.

faltan <- setdiff(t_cancer$variable, t_go$ensembl_gene_id)
x <- matrix(data=rep("", length(faltan)), byrow=TRUE, nrow=1)
colnames(x) <- faltan
x <- as.data.frame(x, stringsAsFactors=FALSE)
go_term <- cbind(go_term, x)

#ponerle a t_cancer la cuenta
total_c <- aggregate(t_cancer$cancer_type, by=list(cancer_type=t_cancer$cancer_type), FUN=length)
t_cancer <- merge(t_cancer, total_c)
colnames(t_cancer)[3] <- "total"

t_cancer$value <- "X"
cancer <- dcast(data=t_cancer, cancer_type + total ~ variable, value.var="value", fill="")

write.table(cancer[, c("cancer_type", "total",  a_by_cancer$variable)], file="81_go_gene_cancertype.txt", quote=FALSE, sep="\t", row.names=FALSE)
#agregar fila vacía
write.table(data.frame(x=" "), file="81_go_gene_cancertype.txt", quote=FALSE, sep="\t", row.names=FALSE, col.names=FALSE, append = TRUE)
write.table(go_term[, c("go", "total", a_by_cancer$variable)], file="81_go_gene_cancertype.txt", quote=FALSE, sep="\t", row.names=FALSE, append = TRUE)

