#datos para grafico TMB
load("40_datos_density_mutations.Rdata")

#datos de genes en comun
load("40_gene_comun.Rdata")
#media por sample
u <- unique(sample_gene[, c(1:3)])
u$sample <- as.character(u$sample)
u2 <- aggregate(u$gene_comun, by=list(cancer_type=u$cancer_type, sample=u$sample), FUN=median)
colnames(u2)[3] <- "median_gene_by_sample"
#media entre las samples
y1 <- aggregate(u2$median_gene_by_sample, by=list(cancer_type = u2$cancer_type), FUN=median)
colnames(y1)[2] <- "median_cancer_by_sample"

count_mutation_per_sample <- merge(count_mutation_per_sample, y1)
#redondear datos
count_mutation_per_sample$median <- round(count_mutation_per_sample$median, digits = 0)
count_mutation_per_sample$median_cancer_by_sample <- round(count_mutation_per_sample$median_cancer_by_sample, digits = 0)

#hacer el gráfico de violin de las mutaciones.
library("ggplot2")
library("ggridges")
p <- ggplot(count_mutation_per_sample, aes(x = number_mutations, y = cancer_type, group=cancer_type))
p <- p + geom_density_ridges(jittered_points = FALSE, position = "raincloud", alpha = .4, scale=1, fill="#0000ff")
p <- p + scale_x_continuous(name="Number of mutations", limits=c(1, 120000), trans="log", breaks=c(1, 2, 5, 10, 20, 50, 100, 200, 500,1000, 2000, 5000,10000, 20000))
p <- p + scale_y_discrete(name="Cancer type", expand = c(.02, 0))
p <- p + geom_text(aes(label=total_samples, y=cancer_type, x=9000, hjust=1, vjust=0), colour="#000000", show.legend=FALSE, check_overlap = TRUE )
p <- p + geom_text(aes(label=median, y=cancer_type, x=35000, hjust=1, vjust=0), colour="#FF0000", show.legend=FALSE, check_overlap = TRUE )
p <- p + geom_text(aes(label=median_cancer_by_sample, y=cancer_type, x=120000, hjust=1, vjust=0), colour="#0000FF", show.legend=FALSE, check_overlap = TRUE )
p <- p + theme(axis.text.x = element_text(angle = 90, hjust=1, vjust=0.5, size=10))

ggsave("40_plot_densities_TMB_common.png", device="png", plot=p, scale = 1,  dpi = 300, width=5, height=8, units="in" )

