load("10_whole_cosmic_fix_new.Rdata")
s <- unique(whole_cosmic_fixed[, c("ID_sample", "cancer_type")])
s <- aggregate(s$ID_sample, by=list(cancer_type = s$cancer_type), FUN=length)
colnames(s)[2] <- "total_samples"

t <- unique(whole_cosmic_fixed[, c("ID_sample", "gene_ensembl", "cancer_type")])
t$gene_ensembl <- as.character(t$gene_ensembl)
t$ID_sample <- as.character(t$ID_sample)
sample_gene <- data.frame()
rm(whole_cosmic_fixed)
gc()
for( cancer in as.character(unique(s$cancer_type))){
print(cancer)
u <- unique(t[t$cancer_type %in% cancer, c("ID_sample", "gene_ensembl")])
y <- unique(u$ID_sample)

samples <- lapply(as.list(y), FUN=function(X){
	u[u$ID_sample %in% X, "gene_ensembl"]
})

oo <- lapply(as.list(1:(length(y) - 1)), FUN=function(X){
	print(X)	
	l <- c((X+1):length(y))
	data.frame(s1 = y[X], s2 = y[l], gene_comun=unlist(lapply(as.list(l), FUN=function(Y){
		length(intersect(samples[[X]], samples[[Y]]))
	})))
})

lista_samples <- Reduce(rbind, oo)
rm(oo)
gc()
print(head(lista_samples))
max_k <- max(lista_samples$gene_comun)

samples <- lapply(as.list(1:max_k), FUN=function(X){
	data.frame(gene_comun = X, sample=unique(c(as.character(lista_samples[lista_samples$gene_comun >= X, "s1"]), as.character(lista_samples[lista_samples$gene_comun >= X, "s2"]))))
})
samples <- Reduce(rbind, samples)
samples$cancer_type <- cancer
sample_gene <- rbind(sample_gene, samples)
}

sample_gene <- merge(sample_gene, s)
load("40_datos_density_mutations.Rdata")
#poner el mismo orden de cancer types (levels) para que queden igual ordenados
z <- levels(count_mutation_per_sample$cancer_type)
sample_gene$cancer_type <- factor(sample_gene$cancer_type, levels=z)

# agregar media mutacional
y <- unique(count_mutation_per_sample[, c("cancer_type", "median")])
sample_gene <- merge(sample_gene, y)
save(sample_gene, file="40_gene_comun.Rdata")

t <- unique(sample_gene[, c(1,5)])

u <- unique(sample_gene[, c(1:3)])
u$sample <- as.character(u$sample)

u2 <- aggregate(u$gene_comun, by=list(cancer_type=u$cancer_type, sample=u$sample), FUN=summary)
u2 <- data.frame(unclass(u2))
colnames(u2)[3:8] <- c("min_gene", "q1_gene", "median_gene", "mean_gene", "q3_gene", "max_gene")

y1 <- aggregate(u2$median_gene, by=list(cancer_type = u2$cancer_type), FUN=summary)
y1 <- data.frame(unclass(y1))
colnames(y1)[2:7] <- c("min_cancer", "q1_cancer", "median_cancer", "mean_cancer", "q3_cancer", "max_cancer")

u2 <- merge(u2, y1)
u2 <- merge(u2, t)
library("ggplot2")
library("ggridges")
p <- ggplot(u2, aes(x = median_gene, y = cancer_type, fill=cancer_type))
p <- p + geom_density_ridges(jittered_points = FALSE, position = "raincloud", alpha = .4, scale=1)
p <- p + scale_x_continuous(name="number of common mutant genes", limits=c(1, 2000), trans="log", breaks=c(0,1,2,5, 10, 20, 50, 100, 200, 500, 1000, 2000))
p <- p + scale_y_discrete(name="cancer type", expand = c(.02, 0))
p <- p + scale_fill_manual(values = rep("#0000ff", times=42), guide = "none")
p <- p + theme(axis.text.x = element_text(angle = 90, hjust=1, vjust=0.5, size=10))
p <- p + geom_text(aes(label=median_cancer, y=cancer_type, x=2000, hjust=1, vjust=0, colour="blue"), show.legend=FALSE, check_overlap = TRUE )
p <- p + scale_colour_manual(values =c("blue") , guide = "none")
ggsave("40_plot_densities_gene_comun.png", device="png", plot=p, scale = 1,  dpi = 300, width=5.7, height=10, units="in" )

